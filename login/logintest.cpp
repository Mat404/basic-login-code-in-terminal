#include <iostream>
//io functions
#include <fstream>
//access/create/write files
#include <string>
//read files with getLine
using namespace std;

int main()
{
    
    int accountCount = 0;
    int counter;
    string Username;
    string Password;
    fstream myFile;
    string tempUsername;
    string tempPassword;
    string line;
    string acctFlag = "[ACCOUNT]";
    bool usernameCorrect = false;
    bool passwordCorrect = false;
    bool usernameFound = false;
    bool accountFound = false;

    myFile.open("Test.txt", ios::in);
    while (getline(myFile, line))
    {
        if (line == acctFlag)
        {
            accountCount++;
        }
    }
    myFile.close();

    int menuselect;
    cout << "Menu:\n";
    cout << "[1] Register:\n";
    cout << "[2] Login:\n";
    cout << "[3] Check Accounts:\n";
    cout << "----------------------------------------\n";
    cin >> menuselect;

    switch (menuselect)
    {
    case 1:

        cout << "Username: \n";
        cin >> Username;
        cout << "Password: \n";
        cin >> Password;

        myFile.open("Test.txt", ios::app);
        //Write text in file
        if (myFile.is_open())
        {
            myFile << acctFlag << endl;
            myFile << Username << endl;
            myFile << Password << endl;
            myFile.close();
        }
        cout << "Successfully registered!\n";
        break;

    case 2:

        cout << "Log in:\n";
        cout << " \n";
        cout << "Username:\n";
        cin >> tempUsername;
        cout << "Password:\n";
        cin >> tempPassword;
        myFile.open("Test.txt", ios::in);
        //read text from file

        while (getline(myFile, line))
        {
            if (accountFound = true)
            {
                // cout << "Checking line" << endl;
                if (line == tempUsername)
                {
                    usernameCorrect = true;
                    // cout << "  -  Username matches\n";
                    usernameFound = true;
                    counter = 2;
                }
                else if (line == tempPassword && counter == 1)
                {
                    passwordCorrect = true;
                    // cout << "  -  Password matches\n";
                }
                if (usernameCorrect == true && passwordCorrect == true)
                {
                    cout << "Username and Password are both correct\n";
                    break;
                }
            }
            if (line == acctFlag || usernameFound == true)
            {
                accountFound = true;
                // cout << "Account Found\n";
            }
            else
            {
                accountFound = false;
                // cout << "Account Not Found\n";
            }
            counter--;
        }
        myFile.close();

        if (usernameCorrect == true && passwordCorrect == true)
        {
            cout << "Successfully logged in!\n";
            return 0;
        }
        else if (usernameCorrect == true && passwordCorrect == false)
        {
            cout << "Incorrect password\n";
            return -1;
        }
        else
        {
            cout << "Username or password was incorrect\n";
            return 0;
        }
        break;
    case 3:
        cout << "----------------------------------------\n";
        cout << accountCount;
        cout << " account(s) registered\n";
        return 0;
        break;
    default:
        cout << "Invalid choice\n";
        return -1;
        break;
    }
}