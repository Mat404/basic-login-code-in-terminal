#include <iostream>
#include <cstdlib>
#include <string>
#include <time.h>
using namespace std;

int main()
{
    // Variable Brick
    int guessedNumb, placeNumber, randomNumb = 0, guesscount = 0, prevNumber = 0;
    long double dispNumber = 0, tempVar1 = 0, tempVar2 = 1;
    string numberSuffix, menuselect;
    bool numberGuessed;
    //-----------------------------------
    cout << "What would you like to do?\n";
    cout << "[Fibonacci] Fibonacci Sequence\n";
    cout << "[Game] Guessing Game\n";
    cin >> menuselect;
    if (menuselect == "Fibonacci")
    {
        cout << "Your Choice: Fibonacci\n";
        cout << "Activating Fibonacci\n";
        cout << "Enter the nth number you want to compute: ";
        cin >> placeNumber;

        switch (placeNumber)
        {
        case 1:
            numberSuffix = "st";
            break;
        case 2:
            numberSuffix = "nd";
            break;
        case 3:
            numberSuffix = "rd";
            break;
        default:
            numberSuffix = "th";
            break;
        }

        if (placeNumber < 1)
        {
            cout << "Invalid Input\n";
            return -1;
        }
        
        for (int tempCount = 0; tempCount < placeNumber - 1; tempCount++)
        {
            dispNumber = tempVar1 + tempVar2;
            tempVar2 = tempVar1;
            tempVar1 = dispNumber;
        }
        cout << "The " << placeNumber << numberSuffix << " Fibonacci number is " << dispNumber << endl;
        return 0;
    }
    else if (menuselect == "Game")
    {
        srand(time(nullptr));
        randomNumb = rand() % 100;
        cout << "Your choice: Game\n";
        cout << "Playing guess the number game\n";
        cout << "================================================================\n";
        while (numberGuessed != true)
        {
            prevNumber = guessedNumb;
            cout << "Guess a random number between 0 and 99: ";
            cin >> guessedNumb;
            if (guessedNumb == prevNumber)
            {
                guesscount--;
            }
            if (guessedNumb > randomNumb)
            {
                cout << "Number is lower\n";
                guesscount++;
            }
            else if (guessedNumb < randomNumb)
            {
                cout << "Number is higher\n";
                guesscount++;
            }
            else if (guessedNumb == randomNumb)
            {
                numberGuessed = true;
                cout << "You guessed the correct number. You tried " << guesscount + 1 << " times\n";
                return 0;
            }
        }
    }
    else
    {
        cout << "Invalid Input\n";
        return -1;
    }
}